# Forge + Bitbucket Pipelines

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)

This repository is an example of how you could set up your Forge app's repository
with Bitbucket Pipelines to automatically deploy your Forge app.

Pipelines is the CI/CD feature of Bitbucket Cloud, and each Bitbucket plan includes
different pricing for Pipelines usage. [Learn more about Pipelines pricing](https://bitbucket.org/product/features/pipelines).

## Installation

If this is your first time using Forge, our
[Getting started](https://developer.atlassian.com/platform/forge/set-up-forge/)
guide will help you install the prerequisites.

If you already have a Forge environment set up, you can deploy this example straight
away. Visit our [Example apps](https://developer.atlassian.com/platform/forge/example-apps/)
page for installation steps.

### Configure your Forge app

Bitbucket Pipelines automates the Forge CLI commands you would normally need to
run to deploy your app to an environment.

Before you can use the CLI without interruption, you'll need to configure your
app's settings. If you don't set these values, the Forge CLI will prompt for
input and block the build.

Run the following command from your app's directory and configure your preferences:

```sh
forge settings set usage-analytics true|false
```

### Setting up Bitbucket Pipelines

Bitbucket Pipelines uses config-as-code. In order to change what your pipeline
can do, you must edit the `bitbucket-pipelines.yml` file. The main benefit of this is
consistency: you'll get the same configuration between commits. You can copy
this file into any repository, and get the same behavior in Pipelines.

Note: You'll need admin access to the Bitbucket repository to configure Bitbucket
Pipelines.

1. Add the Forge CLI as a `devDependency` in the `package.json` file. This has
  already been done in this repository. But, if you want to use this repository's
  pipeline in a different Forge app repository, you'll need to run this command:

    ```sh
    npm install --save-dev @forge/cli
    ```

    This allows Pipelines to get the Forge CLI when it runs `npm install`.

1. Configure two repository variables. Select Repository settings >
  Pipelines > Repository variables.

    Add your Atlassian account email address as a variable named `EMAIL`, and add an
    Atlassian account API token as a secure variable named `TOKEN`.

    This enables Pipelines to run Forge commands on your behalf and gain access to
    your app.

    It is highly recommended to generate a new API token exclusively for your
    pipeline. That way, if you accidentally leak your token, it can be revoked and
    regenerated and only your pipeline needs to be changed.
    Generate a new API token at
    [https://id.atlassian.com/manage/api-tokens](https://id.atlassian.com/manage/api-tokens).

    ![How to access Repository Variables](./docs/images/repo-variables.png)

1. Push a copy of `bitbucket-pipelines.yml` to the root of your git repository.
1. Enable Bitbucket Pipelines in your repository. Select Repository Settings >
  Pipelines > Settings then toggle the *Enable Pipelines* setting.

## Documentation

This Forge app is unchanged from the "Hello world" template generated when you
run `forge create`. In this example app there is an additional file - [bitbucket-pipelines.yml](./bitbucket-pipelines.yml).

In that file, we've specified a `default` pipeline to run for all branches not
specified under the `branches` section. We have only specified one branch under
`branches`: the `master` branch.

Here's how we've configured Pipelines for this repository: all development
branches will deploy to the Forge development environment, whereas `master` will
deploy to staging, then deploy to production when the user triggers it in the
Pipelines UI.

Let's explore the `default` pipeline in detail:

```yaml
# For development branches
default:
  - step:
      name: Run tests
      script:
        - npm install
        - npm run test
  - step:
      name: Deploying to development
      script:
        - npm install
        - node_modules/.bin/forge login -u $EMAIL -t $TOKEN
        - node_modules/.bin/forge deploy -e development
```

Pipelines are made of multiple steps, and steps are made of sequential commands
to execute.

If any command returns a non-zero exit status, the entire pipeline will terminate
and fail. We rely on this behavior during the first step: *Run tests*. First we
install dependencies, then run tests. If the tests fail, the `npm run test`
command will return a non-zero exit code, and terminate the pipeline. This
prevents a failing build from executing the next step: *Deploying to development*.

During the *Deploying to development* stage, we first run `npm install` again,
since we must assume each step is run in isolation. Then, we log in to the Forge
CLI using our credentials, followed by deploying. Since we added the Forge CLI as
a dev dependency, we can rely on it being in the `node_modules/.bin` folder.

Now, let's look at what we do for the `master` branch:

```yaml
# In master, we want to deploy to staging and prod
branches:
  master:
    - step:
        name: Run tests
        script:
          - npm install
          - npm run test
    - step:
        name: Deploying to staging
        deployment: staging
        script:
          - npm install
          - node_modules/.bin/forge login -u $EMAIL -t $TOKEN
          - node_modules/.bin/forge deploy -e staging
    - step:
        name: Deploying to production
        trigger: manual
        deployment: production
        script:
          - npm install
          - node_modules/.bin/forge login -u $EMAIL -t $TOKEN
          - node_modules/.bin/forge deploy -e production
```

The steps are very similar to the default pipeline: it also runs tests, logs in
to the Forge CLI, and deploys. The differences are we specify staging as the
environment to deploy to automatically, and we've added an additional step to
deploy to production. The production step must be started through the Pipelines
UI, as the `trigger: manual` option suggests.

For more details about Bitbucket Pipelines, please read our [Bitbucket Pipelines Documentation](http://go.atlassian.com/BBPipelines).

## Contributions

Contributions welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details.

## License

Copyright (c) 2024 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.